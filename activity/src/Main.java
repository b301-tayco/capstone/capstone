public class Main {
    public static void main(String[] args){
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe");
        contact1.setContactNumber("+639152468536");
        contact1.setAddress("Quezon City");


        phonebook.addContact(contact1);

        Contact contact2 = new Contact("Jane Deo");
        contact2.setContactNumber("+639162148573");
        contact2.setAddress("Caloocan City");

        phonebook.addContact(contact2);

        if(phonebook.isEmpty()){
            System.out.println("Phonebook is empty");
        } else {
          for (Contact contact : phonebook.getContacts()){
              System.out.println(contact.getName());
              System.out.println("-----------------");
              System.out.println(contact.getName() + " has the following registered number: ");
              System.out.println(contact.getContactNumber());
              System.out.println(contact.getName() + " has the following registered address: ");
              System.out.println(contact.getAddress());
              System.out.println( );

          }






    }

}}